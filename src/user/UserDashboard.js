import React, { useEffect, useState } from 'react';
import Base from "../core/Base";
import Table from "../components/table";
import { userOrder } from "../core/helper/orderHelper";
import { isAuthenticated } from "../auth/helper";


const UserDashboard = () => {
    const userId = isAuthenticated() && isAuthenticated().user.id;
    const token = isAuthenticated() && isAuthenticated().token;
    const [orderdata, setOrderData] = useState([]);

    useEffect(() => {
        userOrder(userId, token)
            .then(response => {
               if(response.data){
                setOrderData(response.data)
               }
            })
            .catch(err => {
               console.log(err)
            })

    }, [])
    let head = ["Transaction Id","Product Names","Total Products","Total Amount"];

    return (
        <Base title="User Dashboard">
            <h2>Transaction Info </h2>
                <Table head={head} orderdata={orderdata} />
        </Base>
    )
}

export default UserDashboard;