import React, { useState } from 'react';
import Base from "../core/Base";
import { Redirect } from "react-router-dom";
import { signin, authenticate, isAuthenticated } from "../auth/helper";


const Signin = () => {
    const [values, setValues] = useState({
        email: "",
        password: "",
        error: "",
        success: false,
        loading: false,
        errorMsg: "",
        didRedirect: false
    });

    let { email, password, error, errorMsg, loading } = values;

    const handelChange = (name, event) => {
        setValues({ ...values, error: false, [name]: event.target.value })
    };


    const errorMessage = () => {
        return (
            <div className="row">
                <div className="col-md-6 offset-sm-3 text-left">
                    <div className="alert alert-danger"
                        style={{ display: error ? "" : "none" }}>
                       {errorMsg}
                    </div>
                </div>
            </div>
        )
    }

    const performRedirect = () => {
        if (isAuthenticated()) {
            return <Redirect to="/" />
        }
    };

    const loadingMesage = () => {
        return (
            loading && (
                <div className="alert alert-info">
                    <h1>Loading.....</h1>

                </div>
            )
        )
    }

    const onSubmit = (event) => {
        event.preventDefault();
        setValues({
            ...values, error: false,
            loading: true
        })
        signin({ email, password })
            .then(data => {
                console.log("data", data);
                if (data.token) {
                    //    let sessionToken = data.token;
                    authenticate(data, () => {
                        console.log("token find");
                        setValues({
                            ...values,
                            didRedirect: true
                        })
                    })
                } else {
                    setValues({
                        ...values,
                        error: true,
                        errorMsg: data.error,
                        loading: false,
                    })
                }
            })
            .catch(err => {
                console.log("error", err)
            })
    }

    const signInForm = () => {
        return (
            <div className="row">
                <div classNmae="col-md-6 offset-sm-3 text-left">
                    <form>
                        <div className="form-group">
                            <lable className="text0light">Email</lable>
                            <input className="form-control"
                                value={email}
                                onChange={event => handelChange("email", event)}
                                type="text" />
                        </div>
                        <div className="form-group">
                            <lable className="text0light">Password</lable>
                            <input className="form-control"
                                value={password}
                                onChange={event => handelChange("password", event)}
                                type="password" />
                        </div>
                        <button onClick={onSubmit} className="btn btn-success btn-block">Submit</button>
                    </form>

                </div>

            </div>
        )
    }

    return (
        <Base title="Sign In page" description="Sign In for Shop">
            {loadingMesage()}
            {errorMessage()}
            {signInForm()}
            {performRedirect()}

        </Base>
    )
}

export default Signin;