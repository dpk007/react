import React, { useState } from 'react';
import Base from "../core/Base";
import { Link } from "react-router-dom";
import { signup } from "../auth/helper";


const Signup = () => {
    const [values, setValues] = useState({
        name: "",
        email: "",
        password: "",
        error: "",
        errorMsg: "",
        success: false,
    });

    const { name, email, password, success, error, errorMsg } = values;

    const handelChange = (name, event) => {
        setValues({ ...values, error: false, [name]: event.target.value })
    };

    const successMessage = () => {
        return (
            <div className="row">
                <div className="col-md-6 offset-sm-3 text-left">
                    <div className="alert alert-success"
                        style={{ display: success ? "" : "none" }}>
                        new account created successfully, please <Link to="/signin">login</Link> now.
                    </div>
                </div>
            </div>
        )
    }

    const errorMessage = () => {
        return (
            <div className="row">
                <div className="col-md-6 offset-sm-3 text-left">
                    <div className="alert alert-danger alert-dismissible fade show"
                        style={{ display: error ? "" : "none" }} role="alert">
                        {errorMsg}
                    </div>
                </div>
            </div>
        )
    }

    const onSubmit = (event) => {
        event.preventDefault();
        setValues({ ...values, error: false })
        signup({ name, email, password })
            .then(data => {
                console.log("data", data);
                if (data.email === email) {
                    setValues({
                        ...values,
                        name: "",
                        email: "",
                        password: "",
                        error: "",
                        success: true
                    })
                } else {
                    let errorMessage = ""
                    if (data.name && Array.isArray(data.name)) {
                        errorMessage = "Name: " + data.name[0]
                    } else if (data.email && Array.isArray(data.email)) {
                        errorMessage = "Email: " + data.email[0]
                    } else if (data.password && Array.isArray(data.password)) {
                        errorMessage = "Password: " + data.password[0]
                    }
                    setValues({
                        ...values,
                        error: true,
                        errorMsg: errorMessage,
                        success: false
                    })
                }
            })
            .catch(err => {
                console.log("error", err)
            })
    }

    const signUpForm = () => {
        return (
            <div className="row">
                <div classNmae="col-md-6 offset-sm-3 text-left">
                    <form>
                        <div className="form-group">
                            <lable className="text0light">Name</lable>
                            <input className="form-control"
                                value={name}
                                onChange={event => handelChange("name", event)}
                                type="text" />
                        </div>
                        <div className="form-group">
                            <lable className="text0light">Email</lable>
                            <input className="form-control"
                                value={email}
                                onChange={event => handelChange("email", event)}
                                type="text" />
                        </div>
                        <div className="form-group">
                            <lable className="text0light">Password</lable>
                            <input className="form-control"
                                value={password}
                                onChange={event => handelChange("password", event)}
                                type="password" />
                        </div>
                        <button onClick={onSubmit} className="btn btn-success btn-block">Submit</button>
                    </form>

                </div>

            </div>
        )
    }

    return (
        <Base title="Sign Up page" description="Sign Up for Shop">
            {errorMessage()}
            {successMessage()}
            {signUpForm()}
        </Base>
    )
}

export default Signup;