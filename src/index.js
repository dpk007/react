import React from 'react';
import ReactDOM from 'react-dom';
import './style.css'
import Routes from './Routes.js'

ReactDOM.render(
   <Routes/>,
   document.getElementById("root") 
)