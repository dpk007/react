import React, { useState, useEffect } from 'react';
import Base from './Base';
import { loadCart } from "./helper/cartHelper"
import {Card} from './Card';
import PaymentB from './PaymentB';



const Cart = () => {

   const [products, setProducts] = useState([])
   const [reload, setReload] = useState(false)
   const [success, setSuccess] = useState(false);

   useEffect(() => {
      setProducts(loadCart())
   }, [reload])

   const loadAllProducts = (products) => {

      return (
         <div>
            {products.map((product, index) => {
               return <Card
                  key={index}
                  product={product}
                  removeFromCart={true}
                  addtoCart={false}
                  reload={reload}
                  setReload={setReload}
               />
            })}
            {products.length === 0 ? "No products are added in cart" : ""}
         </div>
      )
   }

   const successMesage = () => {
      return (
              success && <div className="alert alert-success text-center">
                  <h3>Your Order is placed successfully!</h3>
              </div>
          )
  }

   return (
      <Base>
         {successMesage()}
         <div className="row text-center">
            <div className="col-6">
               {products.length > 0 ? loadAllProducts(products) : (
                  <h4>No Products</h4>
               )}
            </div>
            <div className="col-6">
               {products.length > 0 ? 
                <PaymentB products={products} setSuccess={setSuccess} setReload={setReload} /> : 
                (
                  <h3>Please Login or Add somthing on cart</h3>
               )
               }
            </div>
         </div>
      </Base>
   )
}

export default Cart;