import React from 'react';
import { Link, withRouter } from "react-router-dom";
import { signout, isAuthenticated } from "../auth/helper"

const currentTab = (history, path) => {
    if (history.location.pathname === path) {
        return { color: "#2ecc72" }
    } else {
        return { color: "#fff" }
    }
}


const Menu = ({ history, path }) => {
    return (
        // <div>
        //     <ul className="nav nav-tabs bg-dark">
        //         <li className="nav-item">
        //             <Link style={currentTab(history, "/")} className="nav-link" to="/">Home</Link>
        //         </li>
        //         {!isAuthenticated() && (
        //             <>
        //                 <li className="nav-item">
        //                     <Link style={currentTab(history, "/signin")} className="nav-link" to="/signin">SignIn</Link>
        //                 </li>
        //                 <li className="nav-item">
        //                     <Link style={currentTab(history, "/signup")} className="nav-link" to="/signup">SignUp</Link>
        //                 </li>
        //             </>
        //         )}

        // {isAuthenticated() && (
        //     <li className="nav-item">
        //         <Link style={currentTab(history, "/user/dashboard")} className="nav-link" to="/user/dashboard">DashBoard</Link>
        //     </li>
        // )}

        //         <li className="nav-item">
        //             <Link style={currentTab(history, "/cart")} className="nav-link" to="/cart">Cart</Link>
        //         </li>
        // {isAuthenticated() && (
        //     <li className="nav-item">
        //         <span onClick={() => {
        //             signout(() => {
        //                 history.push("/")
        //             })
        //         }}
        //             className="nav-link text-warning">SignOut</span>
        //     </li>
        // )}
        //     </ul>
        // </div>
        <nav class="navbar navbar-expand-lg navbar-light bg-dark">
            <Link style={currentTab(history, "/")} className="nav-link" to="/">Home</Link>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    {!isAuthenticated() && (
                        <>
                            <li class="nav-item">
                                <Link style={currentTab(history, "/signin")} className="nav-link" to="/signin">SignIn</Link>
                            </li>
                            <li class="nav-item">
                                <Link style={currentTab(history, "/signup")} className="nav-link" to="/signup">SignUp</Link>
                            </li>
                        </>
                    )}
                    {isAuthenticated() && (
                        <li className="nav-item">
                            <Link style={currentTab(history, "/user/dashboard")} className="nav-link" to="/user/dashboard">dashboard</Link>
                        </li>
                    )}
                    {isAuthenticated() && (
                        <li className="nav-item nav-link text-warning" onClick={() => {
                            signout(() => {
                                history.push("/")
                            })
                        }} >
                            SignOut
                        </li>
                    )}
                </ul>
                <Link style={currentTab(history, "/cart")} className="nav-link" to="/cart">Cart</Link>
            </div>
        </nav>
    )
}

export default withRouter(Menu);