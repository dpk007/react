import React, { useState, useEffect } from "react"
import { Redirect, Link } from "react-router-dom"
import { cartEmpty } from "./helper/cartHelper"
import { getmeToken, processPayment } from "./helper/paymentHelper";
import { createOrder } from "./helper/orderHelper";
import { isAuthenticated, signout } from "../auth/helper";

import DropIn from "braintree-web-drop-in-react"

const PaymentB = ({
    products,
    reload = undefined,
    setReload = f => f,
    setSuccess = f => f,
}) => {

    const [info, setInfo] = useState({
        loading: '',
        success: false,
        clientToken: null,
        error: "",
        instance: {},
    })

    const token = isAuthenticated() && isAuthenticated().token;

    const getToken = (token) => {
        if (token === false) {
            // TODO
        } else {
            getmeToken(token)
                .then(info => {
                    if (info.error) {
                        setInfo({
                            ...info,
                            error: info.error
                        })
                        signout(() => {
                            return <Redirect to="/login" />;
                        })
                    } else {
                        const clientToken = info.clientToken;
                        setInfo({ clientToken })
                    }
                })
        }
    }

    useEffect(() => {
        getToken(token);
    }, [])

    const getAmount = () => {
        let amount = 0;
        products.map(p => (
            amount = amount + parseInt(p.price)
        ));
        return amount;
    }

    const onPurchase = () => {
        setInfo({ loading: true })
        let nonce;
        info.instance.requestPaymentMethod()
            .then(data => {
                nonce = data.nonce;
                const paymentData = {
                    paymentMethodNonce: nonce,
                    amount: getAmount(),
                };
                processPayment(token, paymentData)
                    .then(response => {
                        if (response.error) {
                            if (response.code === '1') {
                                console.log("PAYMENT FAILED");
                                signout(() => {
                                    return <Redirect to="/" />
                                })
                            }
                        } else {
                            setInfo({
                                ...info,
                                success: response.success, loading: false
                            })
                            console.log("PAYMENT SUCCESS")
                            let product_names = ""
                            let count = 0
                            products.forEach(function (item) {
                                if(count === 0){
                                   product_names += item.name
                                   count++;
                                }else{
                                    product_names += ", "+ item.name 
                                }
                            }) 
                            const orderData = {
                                products: product_names,
                                transaction_id: response.transaction.id,
                                amount: response.transaction.amount,
                            }

                            createOrder(token, orderData)
                                .then(response => {
                                    if (response.error) {
                                        if (response.code === "1") {
                                            console.log("ORDER FAILED");
                                        }
                                        signout(() => {
                                            return <Redirect to="/" />
                                        })
                                    } else {
                                        if (response.success === true) {
                                            console.log("ORDER PLACED")
                                            setReload(true)
                                            setSuccess(true)
                                        }
                                    }
                                })
                                .catch(error => {
                                    setInfo({ loading: false, success: false })
                                    console.log("ORDER FAILED");
                                })
                            cartEmpty(() => {
                                console.log("CART IS EMPTYED OUT")
                            })
                        }
                    })
                    .catch()
            })
            .catch(e => console.log("Nonce", e))

    }

    const showbtnDropIn = () => {
        return (
            <div>
                {
                    info.clientToken !== null && products.length > 0 ?
                        (
                            <div>
                                <DropIn
                                    options={{ authorization: info.clientToken }}
                                    onInstance={instance => (info.instance = instance)} />
                                <button onClick={() => onPurchase()} className="btn btn-block btn-success">BUY</button>
                            </div>
                        ) :
                        (
                            <h3>Please <Link to="/signin">Login</Link> for checkout!</h3>
                        )
                }
            </div>
        )
    }

    return (
        <div>
            <h1>Your Bill is $ {getAmount()}</h1>
            {showbtnDropIn()}
        </div>
    )
}

export default PaymentB;