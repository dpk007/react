import React, { useState } from 'react';
import ImageHelper from './helper/ImageHelper';
import { Redirect } from 'react-router-dom';
import { isAuthenticated } from '../auth/helper'
import { addItemToCart, removeItemFromCart } from './helper/cartHelper';


export const Card = ({ product,
    addToCart,
    removeFromCart,
    reload = undefined,
    setReload = (f) => f
}) => {
    const [redirect, setRedirect] = useState(false);

    const cartTitle = product ? product.name : 'A photo from me';
    const cartDescription = product ? product.description : 'Description';
    const cartPrice = product ? product.price : 'Default';

    const addToCartFnc = () => {
        if (isAuthenticated) {
            addItemToCart(product, () => setRedirect(true))
        } else {
            console.log('Please login!');
            return <Redirect to="/login" />
        }
    }

    const getRedirect = (redirect) => {
        if (redirect) {
            return <Redirect to="/cart" />;
        }
    }

    const showAddToCart = addToCart => {
        return addToCart && (
            <button
                onClick={addToCartFnc}
                className="btn btn-block btn-outline-success mt-2 mb-2"
            >
                Add to cart
             </button>
        )
    }

    const showRemoveFromCart = removeFromCart => {
        return removeFromCart && (
            <button
                onClick={() => {
                    removeItemFromCart(product.id);
                    setReload(!reload);
                }
                }
                className="btn btn-block btn-outline-danger mt-2 mb-2"
            >
                Remove from cart
             </button>
        )
    }

    return (
        <div className="card text-white bg-dark border border-info">
            <div className="card-header lead">{cartTitle}</div>
            <div className="card-body">
                <ImageHelper product={product} />
                {getRedirect(redirect)}
                <p className="lead font-weight-normal text-wrap">
                    {cartDescription}
                </p>
                <p className="btn btn-success rounded btn-sm px-4">$ {cartPrice}</p>
                <div className="row">
                    <div className="col-12">
                        {showAddToCart(addToCart)}
                    </div>
                    <div className="col-12">
                        {showRemoveFromCart(removeFromCart)}
                    </div>
                </div>
            </div>
        </div>
    )
}