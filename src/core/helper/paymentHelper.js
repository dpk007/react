import { API } from "../../backend";

export const getmeToken = (token) => {

    return fetch(`${API}payment/gettoken/`, {
        method: "GET",
        headers: {'Authorization': `Token ${token}`}
    })
        .then((response) => {
            return response.json();
        })
        .catch((err) => console.log(err))
};


export const processPayment = (token, paymentInfo) => {
    const formData = new FormData();

    for(const name in paymentInfo){
        formData.append(name, paymentInfo[name])
    }

    return fetch(`${API}payment/process/`,{
        method:"POST",
        headers: {'Authorization': `Token ${token}`},
        body:formData,
    })
    .then((response) => {
        return response.json();
    })
    .catch((err) => console.log(err))
}