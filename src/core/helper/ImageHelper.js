import React from 'react'

const ImageHelper = ({product}) => {
    const imageurl = product ? product.image : '';

    return(
        <div className="rounded borded border-success p-2 text-center">
             <img src={imageurl}
             style={{ maxHeight:'100%', maxWidth:'100%'}}
             className='mb-3 rounded'
             alt=''/>
        </div>
    )
}

export default ImageHelper;