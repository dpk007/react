import { API } from "../../backend";

export const createOrder = (token, orderData) => {

    const formData = new FormData()
    for (const name in orderData) {
        formData.append(name, orderData[name]);
    }

    return fetch(`${API}order/add/`, {
        method: "POST",
        headers: { 'Authorization': `Token ${token}` },
        body: formData
    })
        .then((response) => {
            return response.json();
        })
        .catch((err) => console.log(err))
}

export const userOrder = (userId, token) => {
    return fetch(`${API}order/get_user_order/`, {
        method: "GET",
        headers: { 'Authorization': `Token ${token}` },
    })
        .then((data) => {
            return data.json()
        })
        .catch(e => console.log(e))
}