const addItemToCart = (item, next) => {
    let cart = [];
    if (typeof window !== undefined) {
        cart = localStorage.getItem("cart") ? JSON.parse(localStorage.getItem("cart")) : [];
    }

    cart.push({ ...item, })

    localStorage.setItem("cart", JSON.stringify(cart));
    next();
}

const removeItemFromCart = (productId) => {
    let cart = [];
    if (typeof window !== undefined) {
        if (JSON.parse(localStorage.getItem("cart"))) {
            cart = JSON.parse(localStorage.getItem("cart"));
        }
        cart.map((product, index) => {
            if (product.id === productId) {
                cart.splice(index, 1);
            }
            return product;
        });
        localStorage.setItem("cart", JSON.stringify(cart));
    }
    return cart;
}

const cartEmpty = (next) => {
    if (typeof window !== undefined) {
        if (localStorage.getItem("cart")) {
            localStorage.removeItem("cart");
        }
        let cart = [];
        localStorage.setItem("cart", JSON.stringify(cart));
    }
    next();
}

const loadCart = () => {
    if (typeof window !== undefined) {
        if (localStorage.getItem("cart")) {
            return localStorage.getItem("cart") ? JSON.parse(localStorage.getItem("cart")) : []
        }
        return [];
    }
}

const isItemInCart = (productId) => {
    let cart = [];
    if (typeof window !== undefined) {
        if (JSON.parse(localStorage.getItem("cart"))) {
            cart = JSON.parse(localStorage.getItem("cart"));
        }
        let data = cart.filter((product, index) => { return product.id === productId });
        return data.length > 0 ? true : false;
    }
    return false;
}

export { cartEmpty, addItemToCart, removeItemFromCart, loadCart, isItemInCart }