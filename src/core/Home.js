import React, { useState, useEffect } from 'react';
import { getProducts } from "./helper/coreapicalls";
import Base from './Base';
import "../style.css";
import { Card } from './Card';
import { isItemInCart } from '../core/helper/cartHelper'

function Home() {

    const [products, setProducts] = useState([])
    const [error, setError] = useState(false)
    const [reload, setReload] = useState(false)

    const loadAllProducts = () => {
        try {
            getProducts()
                .then(data => {
                    if (data.error) {
                        setError(data.error);
                    } else {
                        setProducts(data);
                    }
                })
        } catch (e) {
            setError(e);
        }
    }

    useEffect(() => {
        loadAllProducts()
    }, [reload])



    return (
        <Base title="Home page" description="Welcome"
        >
            <div className="row">
                {products && products.map((product, index) => {
                    return (
                        <div key={index} className="col-4 mb-4">

                            <Card addToCart={!isItemInCart(product.id)} 
                                removeFromCart={isItemInCart(product.id)} 
                                product={product} setReload={setReload}
                                reload={reload}/>
                        </div>
                    )
                })}
                {
                    !products && <h2>No Product</h2>
                }
            </div>
        </Base>
    )
}

export default Home;