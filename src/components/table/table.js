import React from 'react';

const table = ({head, orderdata}) => {
    let heads = head.map((data)=> <th scope='col'>{data}</th>)
    let orderinfo = orderdata && orderdata.map((data)=> {
        return <tr>
                    <th scope="row">{data.transaction_id}</th>
                    <td>{data.product_names}</td>
                    <td>{data.total_products}</td>
                    <td>$ {data.total_amount}</td>
                </tr>
    })
    return (
        <table className="table table-striped table-dark">
            <thead>
                <tr>
                  {heads}
                </tr>
            </thead>
            <tbody>
               {orderinfo}
            </tbody>
        </table>
    )
}

export default table;